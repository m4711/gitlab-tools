import datetime
import os
import requests
import sys
import tempfile

if len(sys.argv) != 2:
    print(f'syntax: {sys.argv[0]} <token>')
    exit(1)
token = sys.argv[1]

HEADERS = {"Authorization": "Bearer " + token}

def run_graphql_query(query):
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = HEADERS)
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception(f"Query failed to run by returning code of {request.status_code}. {query}")

all_users = set()

group_query = """
{
  group(fullPath: "%s") {
    id
    descendantGroups(includeParentDescendants: false) {
      edges {
        node {
          fullPath
          path
        }
      }
    }
    projects {
      edges {
        node {
          id
          fullPath
          path
          projectMembers {
            edges {
              node {
                user {
                  username
                }
                accessLevel {
                  stringValue
                }
              }
            }
          }
        }
      }
    }
    groupMembers {
      edges {
        node {
          user {
            username
          }
          accessLevel {
            stringValue
          }
        }
      }
    }
  }
}
"""


def get_direct_members(id, type):
    members = []
    page_number = 1
    while page_number:
        request = requests.get(f"https://gitlab.com/api/v4/{type}s/{id}/members?page={page_number}", headers = HEADERS )
        if request.status_code == 200:
            members += list(map(lambda x: x["username"], request.json()))
        else:
          raise Exception(f"Failed to get direct members of {type} {id} ({request.status_code})")
        page_number = request.headers.get('X-Next-Page')
    return members

def get_direct_group_members(group_id):
    return get_direct_members(group_id, "group")

def get_direct_project_members(project_id):
    return get_direct_members(project_id, "project")

def get_group_info(group_path):
    print(group_path, flush=True)
    group_info = { "name": group_path.split("/")[-1], "path": group_path, "subgroups": [], "projects": [], "members": {}}
    result = run_graphql_query(group_query % group_path)

    # recurse on the subgroups
    for group_node in result["data"]["group"]["descendantGroups"]["edges"]:
        group_info['subgroups'].append(get_group_info(group_node["node"]["fullPath"]))

    # load date of the projects of the group
    for project_node in result["data"]["group"]["projects"]["edges"]:
        project_info = { "name": project_node["node"]["path"], "path": project_node["node"]["fullPath"], "members": {}}
        project_id = project_node["node"]["id"].split("/")[-1]
        project_info["direct-members"] = get_direct_project_members(project_id)
        for member in project_node["node"]["projectMembers"]["edges"]:
            project_info["members"][member["node"]["user"]["username"]] = member["node"]["accessLevel"]["stringValue"]
            all_users.add(member["node"]["user"]["username"])
        group_info["projects"].append(project_info)

    # load the group data
    group_id = result["data"]["group"]["id"].split("/")[-1]
    group_info["direct-members"] = get_direct_group_members(group_id)
    for member in result["data"]["group"]["groupMembers"]["edges"]:
        if member["node"]["user"]:
            group_info["members"][member["node"]["user"]["username"]] = member["node"]["accessLevel"]["stringValue"]
            all_users.add(member["node"]["user"]["username"])
        else:
            print("The group contains an awaited invited, see https://gitlab.com/groups/" + group_path + "/-/group_members?tab=invited", flush=True)
    return group_info

def write_style():
    report.write("""
        <style>
        table { table-layout: fixed; width: 100%; border-spacing:0;}
        .GUEST { background-color: LightPink}
        .REPORTER { background-color: LightGreen}
        .DEVELOPER { background-color: LemonChiffon}
        .MAINTAINER { background-color: Gold}
        .OWNER { background-color: Tomato}
        .DIRECT { font-weight: 1000; }
        td { border-top: 1px solid white; border-bottom: 1px solid white;}
        tbody tr:hover td { border-color: black;}
        thead { position: sticky; top: 0; }
        th { writing-mode: vertical-lr; white-space: nowrap; position: relative; background-color: White; }
        th div { position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%) rotate(-90deg); text-align: center; }
        </style>
        """)

def write_script():
    report.write("""
        <script>
        let total = 0, nbCol;
        function draw() {
            for (i = 1; i < nbCol; i++) {
                const elements = document.querySelectorAll("tr>:nth-child(" + (i + 1) + ")");
                for (j = 2; j < elements.length; j++) {
                    elements[j].style.color = ((total > 0) && !document.getElementById("cb" + i).checked) ? "#A9A9A9"
                                                                                                          : "black";
                }
            }
        }
        function handle(element) {
            total += (element.checked) ? 1 : -1;
            draw();
        }
        window.onload = (event) => {
            nbCol = document.querySelectorAll("th").length;
        };
        </script>
        """)

def write_legend():
    report.write('This report has been generated on ' +
        datetime.datetime.now().astimezone().isoformat() +
        ' by <a href="https://gitlab.com/m4711/gitlab-tools/-/blob/main/memberOverview.py" target="_blank"">https://gitlab.com/m4711/gitlab-tools/-/blob/main/memberOverview.py</a>.<br>\n' +
        'The GitLab roles and permissions are described in <a href="https://docs.gitlab.com/ee/user/permissions.html" target="_blank"">https://docs.gitlab.com/ee/user/permissions.html</a>.<br>\n')

def write_projet_info(project_info, report, depth):
    report.write('<tr><td>' +
                 ('&nbsp;&nbsp;' * (depth - 1)) +
                 '&#9655;&nbsp;<a href="https://gitlab.com/' +
                 project_info["path"] +
                 '/-/project_members" target="_blank">' +
                 project_info["name"] +
                 '</a></td>')
    for user in sorted_users:
        if user in project_info["members"]:
            directStyle = " DIRECT" if user in project_info["direct-members"] else ""
            directIndicator = "*" if user in project_info["direct-members"] else ""
            report.write('<td class="' + project_info["members"][user] + directStyle + '">' + project_info["members"][user][0] + directIndicator + "</td>")
        else:
            report.write("<td></td>")
    report.write("</tr>\n")

def write_group_info(group_info, report, depth):
    report.write('    <tr><td>' +
                 ('&nbsp;&nbsp;' * (depth - 1)) +
                 ('&#9654;&nbsp;' if (depth > 0) else '') +
                 '<a href="https://gitlab.com/groups/' +
                 group_info["path"] +
                 '/-/group_members" target="_blank">' +
                 group_info["name"] +
                 '</a></td>')
    for user in sorted_users:
        if user in group_info["members"]:
            directStyle = " DIRECT" if user in group_info["direct-members"] else ""
            directIndicator = "*" if user in group_info["direct-members"] else ""
            report.write('<td class="' + group_info["members"][user] + directStyle + '">' + group_info["members"][user][0] + directIndicator + "</td>")
        else:
            report.write("<td></td>")
    report.write("</tr>\n")
    for subgroup in sorted(group_info["subgroups"], key=lambda g: g['name']):
        write_group_info(subgroup, report, depth + 1)
    for project in sorted(group_info["projects"], key=lambda p: p['name']):
        write_projet_info(project, report, depth + 1)


root = get_group_info('henixdevelopment')

sorted_users = sorted(all_users, key=str.lower)

report = tempfile.NamedTemporaryFile(mode="w", suffix=".html", delete=False)

report.write('<!DOCTYPE html>\n<html>\n<head>\n<meta charset="utf-8">\n')
write_style()
report.write('</head>\n<body>\n')
write_script()
write_legend()
report.write('<table>\n')

report.write('<colgroup><col style="width: 15px;"></col>')
for user in sorted_users:
    report.write('<col style="width: 1px;"></col>')
report.write('</colgroup>\n')

report.write("<thead>\n<tr><th></th>")
for user in sorted_users:
    report.write("<th>" + user + "</th>")
report.write("</tr>\n</thead>\n")

report.write("<tbody>\n    <tr><td></td>")
i = 1
for user in sorted_users:
    report.write('<td><input type="checkbox" id="cb' + str(i) + '" onchange="handle(this)"></td>')
    i = i + 1
report.write("</tr>\n")

write_group_info(root, report, 0)
report.write("</tbody>\n</table>\n</body>\n</html>")

os.startfile(report.name)
