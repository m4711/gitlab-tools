import html
import requests
import sys
import tempfile
import webbrowser

import urllib
from projectLists import get_squash_projects

if len(sys.argv) != 2:
    print(f'syntax: {sys.argv[0]} <token>')
    exit(1)
token = sys.argv[1]

HEADERS = {"Authorization": "Bearer " + token}

def run_query(project):
    request = requests.get("https://gitlab.com/api/v4/projects/" + urllib.parse.quote_plus(project), headers = HEADERS)
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception(f"Query failed to run by returning code of {request.status_code}. {request.text}")

def extract_scalar(fields, name, value):
    fields[name] = str(value)

def extract_list(fields, name, list):
    txt = ""
    for value in list:
        if len(txt):
            txt += "\n"
        txt += str(value)
    fields[name] = txt

def extract_dict(fields, name, value):
    prefix = (name + ".") if name else ""
    for field in value:
        if type(value[field]) in (int, bool, str, type(None)):
            extract_scalar(fields, prefix + field, value[field])
        elif type(value[field]) is dict:
            extract_dict(fields, prefix + field, value[field])
        elif type(value[field]) is list:
            extract_list(fields, prefix + field, value[field])
        else:
            raise ValueError("Unsupported type " + str(type(value[field])) + " for " + field)

everything = {}
parsed_projects = []
for project in get_squash_projects(token):
    result = run_query(project)
    d = {}
    extract_dict(d, "", result)
    parsed_projects.append(project)
    everything[project] = d

report = tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", suffix=".html", delete=False)
report.write("""
<!DOCTYPE html>
<html>
<head>
  <META http-equiv="Content-Type" content="text/html; charset=utf-8">
  <style>
    table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
      text-align:left;
      vertical-align:top;
    }
    th {
      background: white;
      position: sticky;
      top: 0;
}
  </style>
  <title>Project overview</title>
</head>
<body>
<table>
  <thead>
    <tr>
      <th></th>
""")
for project in parsed_projects:
    report.write("      <th><a href='https://gitlab.com/" + project + "/edit' target='_blank'>" + html.escape(project) + "</a></th>\n")
report.write("""
    </tr>
  </thead>
  <tbody>
""")
for f in everything[parsed_projects[0]]:
    report.write("    <tr>\n")
    report.write("      <td>" + html.escape(f) + "</td>\n")
    for project in parsed_projects:
        description = html.escape(everything[project][f]).replace("\n","<br/>").replace("\r","") if f in everything[project] else "❌"
        report.write("      <td>" + description + "</td>\n")
    report.write("    </tr>\n")
report.write("""
  </tbody>
</table>
</body>
</html>
""")

webbrowser.open('file://' + report.name)
