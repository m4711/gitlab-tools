#!/bin/bash

# This is a high level script using scripts to generate the report on the issues incorrectly managed and send this report by email

gitlabToken="$1"
mailBook="$2"
smtpUser="$3"
smtpPassword="$4"

python issueChecker.py -hs report.html -ts report.txt -cf culprits.txt $gitlabToken
if [ $? -eq 0 ]; then
    echo "No issue is badly managed"
    exit 0
fi

cat report.txt

mails="lmazure@henix.fr"
while IFS="" read -r culprit
do
    mail=`grep -w "$culprit" "$mailBook" | cut -f2`
    if [ "$mail" = "" ]; then
        echo "Email of user $culprit is unknown"
        exit 1
    fi
    mails="$mails $mail"
done < culprits.txt

python emailSender.py $smtpUser $smtpPassword "GitLab issues badly managed" report.html lmazure@henix.fr $mails
