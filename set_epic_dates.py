import colorama
import datetime
import re
import requests
import sys

release_dates = {
    "Release::6.0"    : ("2023-03-01", "2023-11-15"),
    "Release::7.0"    : ("2023-11-15", "2024-05-15"),
    "Release::8.0"    : ("2024-05-15", "2024-11-15"),
    "Release::9.0"    : ("2024-11-15", "2025-05-15"),
    "Release::10.0"   : ("2025-05-15", "2025-11-15"),
    "Release::SDM_1.0": ("2023-05-01", "2024-01-15"),
}

if len(sys.argv) != 2:
    print(f'syntax: {sys.argv[0]} <token>')
    exit(1)
token = sys.argv[1]

HEADERS = {"Authorization": "Bearer " + token}

def run_graphql_query(query):
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = HEADERS)
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception(f"Query failed to run by returning code of {request.status_code}. {query}")

epic_list_query = """
{
  group (fullPath: "henixdevelopment/squash") {
    epics (state: opened, labelName: ["Roadmap","Release::*"], after: "%s") {
      edges {
        cursor
        node {
          id
          iid
          title
          group {
            fullPath
          }
          startDateIsFixed
          startDateFixed
          dueDateIsFixed
          dueDateFixed
          labels {
            edges {
              node {
                title
              }
            }
          }
        }
      }
    }
  }
}
"""

issue_list_of_epic = """
{
  group (fullPath: "henixdevelopment") {
    issues(includeSubgroups: true, epicId: "%s", includeSubepics: true) {
      nodes {
        milestone {
          id
        }
        title
        webUrl
      }
    }
  }
}
"""

set_epic_date_request ="""
mutation
{
  updateEpic (input: {
    groupPath: "%s",
    iid: "%s",
    startDateIsFixed: true,
    startDateFixed: "%s",
    dueDateIsFixed: true,
    dueDateFixed: "%s"
  }) {
    clientMutationId
  }
}
"""

def set_epic_dates(group_path, iid, start_date, due_date):
    run_graphql_query(set_epic_date_request % (group_path, iid, start_date, due_date))

def get_release_of_epic(epic):
    for label_node in epic['labels']['edges']:
        title = label_node['node']['title']
        if title.startswith("Release::"):
            return title
    raise RuntimeError("Internal error - Cannot get release tag of epic")

def handle_epic(epic):
    group_path = epic['group']['fullPath']
    iid = epic['iid']
    title = epic['title']
    print(f"\n{group_path} {iid} \'{title}\'", flush=True)
    epic_id = re.sub(".*/", "", epic["id"])
    result = run_graphql_query(issue_list_of_epic % epic_id)
    for issue_node in result["data"]["group"]["issues"]["nodes"]:
        if issue_node["milestone"]:
            print(f"epic contains issue \'{issue_node['title']}\' ({issue_node['webUrl']}) which has a milestone, it is left unchanged", flush=True)
            return
    release = get_release_of_epic(epic)
    if release not in release_dates:
        raise RuntimeError(f"Internal error - Unknown release '{release}'")
    today = datetime.datetime.now().strftime("%Y-%m-%d")
    start_date = max(today, release_dates[release][0])
    due_date = max(today, release_dates[release][1])
    if epic['startDateIsFixed'] and epic['dueDateIsFixed'] and (epic['startDateFixed'] == start_date) and (epic['dueDateFixed'] == due_date):
        print("epic has already correct dates, it is left unchanged")
        return
    print(colorama.Fore.YELLOW + f"set epic to start date = {start_date} and due date = {due_date} ({release})" + colorama.Style.RESET_ALL)
    set_epic_dates(group_path, iid, start_date, due_date)

def handle_epics():
    cursor = ""
    while True:
        result = run_graphql_query(epic_list_query % cursor)
        if len(result["data"]["group"]["epics"]["edges"]) == 0:
            break
        for epic_node in result["data"]["group"]["epics"]["edges"]:
            handle_epic(epic_node["node"])
            cursor = epic_node["cursor"]

handle_epics()
