# This is the list of projects and groups considered by the scripts used to manage GitLab issues

import requests

tf_projects = [
                  "henixdevelopment/open-source/code-samples/bdd-robotframework-02",
                  "henixdevelopment/open-source/code-samples/howto-bdd-cucumber-01",
                  "henixdevelopment/open-source/code-samples/howto-bdd-robotframework-01",
                  "henixdevelopment/open-source/opentestfactory/agent",
                  "henixdevelopment/open-source/opentestfactory/ci-common/image-build-trigger",
                  "henixdevelopment/open-source/opentestfactory/ci-common/java-ci",
                  "henixdevelopment/open-source/opentestfactory/ci-common/python-ci",
                  "henixdevelopment/open-source/opentestfactory/images/otf-all-in-one-images",
                  "henixdevelopment/open-source/opentestfactory/java-param-library",
                  "henixdevelopment/open-source/opentestfactory/java-plugins",
                  "henixdevelopment/open-source/opentestfactory/java-toolkit",
                  "henixdevelopment/open-source/opentestfactory/jenkins-plugin",
                  "henixdevelopment/open-source/opentestfactory/micronaut-java-plugins",
                  "henixdevelopment/open-source/opentestfactory/micronaut-java-toolkit",
                  "henixdevelopment/open-source/opentestfactory/orchestrator",
                  "henixdevelopment/open-source/opentestfactory/python-plugins",
                  "henixdevelopment/open-source/opentestfactory/python-toolkit",
                  "henixdevelopment/open-source/opentestfactory/qualitygate",
                  "henixdevelopment/open-source/opentestfactory/reporting-analysis",
                  "henixdevelopment/open-source/opentestfactory/schemas",
                  "henixdevelopment/open-source/opentestfactory/site",
                  "henixdevelopment/open-source/opentestfactory/squash-tf-services",
                  "henixdevelopment/open-source/opentestfactory/test-environments/ci-environment-builder",
                  "henixdevelopment/open-source/opentestfactory/test-environments/maven_environment",
                  "henixdevelopment/open-source/opentestfactory/test-environments/robotframework_environment",
                  "henixdevelopment/open-source/opentestfactory/test-samples/agilitest/agilitest-demo",
                  "henixdevelopment/open-source/opentestfactory/test-samples/agilitest/agilitest-doc-check",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cucumber/cucumber_multimodule",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cucumber/cucumber-special-char",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cucumber/new-cucumber-gestion-stock",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cucumber/new-cucumber-stock-management",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cucumber/old-cucumber-gestion-stock",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cucumber/old-cucumber-stock-management",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cypress/cypress-calc-single-spec",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cypress/cypress-doc-check",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cypress/cypress-nested-proj",
                  "henixdevelopment/open-source/opentestfactory/test-samples/cypress/cypress-param-calc-single-spec",
                  "henixdevelopment/open-source/opentestfactory/test-samples/junit/create-perf-test",
                  "henixdevelopment/open-source/opentestfactory/test-samples/junit/junit-calc-param-lib",
                  "henixdevelopment/open-source/opentestfactory/test-samples/junit/junit-calc",
                  "henixdevelopment/open-source/opentestfactory/test-samples/junit/junit-screenshots",
                  "henixdevelopment/open-source/opentestfactory/test-samples/katalon-api",
                  "henixdevelopment/open-source/opentestfactory/test-samples/katalon-samples-with-space",
                  "henixdevelopment/open-source/opentestfactory/test-samples/katalon-samples",
                  "henixdevelopment/open-source/opentestfactory/test-samples/katalon-web",
                  "henixdevelopment/open-source/opentestfactory/test-samples/postman-openweather",
                  "henixdevelopment/open-source/opentestfactory/test-samples/postman-samples",
                  "henixdevelopment/open-source/opentestfactory/test-samples/ranorex/ranorex-doc-check",
                  "henixdevelopment/open-source/opentestfactory/test-samples/robotframework/robot-db-api",
                  "henixdevelopment/open-source/opentestfactory/test-samples/robotframework/robot-demo-xml",
                  "henixdevelopment/open-source/opentestfactory/test-samples/robotframework/robot-screenshots",
                  "henixdevelopment/open-source/opentestfactory/test-samples/robotframework/robot-special-char",
                  "henixdevelopment/open-source/opentestfactory/test-samples/robotframework/robot-tf-param-check",
                  "henixdevelopment/open-source/opentestfactory/test-samples/skf/skf-compare-xml-param",
                  "henixdevelopment/open-source/opentestfactory/test-samples/skf/skf-compare-xml",
                  "henixdevelopment/open-source/opentestfactory/test-samples/skf/skf-selenium-junit",
                  "henixdevelopment/open-source/opentestfactory/test-samples/soapui-samples",
                  "henixdevelopment/open-source/opentestfactory/test-samples/soapui/soapui-openweather",
                  "henixdevelopment/open-source/opentestfactory/test-samples/uft/uft-doc-check",
                  "henixdevelopment/open-source/opentestfactory/test-samples/uft/uft-param-doc-check",
                  "henixdevelopment/open-source/opentestfactory/test-samples/workflow-example",
                  "henixdevelopment/open-source/opentestfactory/tools",
                  "henixdevelopment/squash/doc/squash-autom-devops-doc-en",
                  "henixdevelopment/squash/doc/squash-autom-devops-doc-fr",
                  "henixdevelopment/squash/squash-autom-devops-data-injection",
                  "henixdevelopment/squash/squash-autom-devops/ci-common/python-ci",
                  "henixdevelopment/squash/squash-autom-devops/integration-tests",
                  "henixdevelopment/squash/squash-autom-devops/java-plugins",
                  "henixdevelopment/squash/squash-autom-devops/premium-python-plugins",
                  "henixdevelopment/squash/squash-autom-devops/python-plugins",
                  "henixdevelopment/squash/squash-autom-devops/reporting",
                  "henixdevelopment/squash/squash-autom-devops/squash-orchestrator",
                  "henixdevelopment/squash/squash-autom-devops/squash-premium-services",
                  "henixdevelopment/squash/squash-autom-devops/test-environments",
                ]


group_query = """
{
  group(fullPath: "%s") {
    descendantGroups(includeParentDescendants: false) {
      edges {
        node {
          fullPath
        }
      }
    }
    projects {
      edges {
        node {
          fullPath
        }
      }
    }
  }
}
"""

groups = [
            "henixdevelopment/open-source",
            "henixdevelopment/squash",
]

def _run_graphql_query(query, token):
    h = {"Authorization": "Bearer " + token}
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = h)
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception(f"Query failed to run by returning code of {request.status_code}. {query}")

def _get_group_projects(group, token):
    projects = []
    result = _run_graphql_query(group_query % group, token)
    for group_node in result["data"]["group"]["descendantGroups"]["edges"]:
        projects.extend(_get_group_projects(group_node["node"]["fullPath"], token))
    for project_node in result["data"]["group"]["projects"]["edges"]:
        projects.append(project_node["node"]["fullPath"])
    return projects

def get_squash_projects(token):
    projects = []
    for group in groups:
        projects.extend(_get_group_projects(group, token))
    return projects
