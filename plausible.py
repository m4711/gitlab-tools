import argparse
import requests
from datetime import datetime, timedelta
import openpyxl

parser = argparse.ArgumentParser()
parser.add_argument("token", help="Plausible token")
parser.add_argument("file", help="name of the Excel file (*.xslx) to generate")
args = parser.parse_args()

def run_get_query(query, token):
    header = {"Authorization": "Bearer " + token}
    request = requests.get(query, headers = header)
    if request.status_code == 200:
        return request.json()
    raise Exception(f"GET Query {query} failed to run by returning code of {request.status_code}. {query}")

PLAUSIBLE_API = "https://plausible.io/api/v1"
SITE_NAME="squashtest.com"
START_DATE = datetime(2022, 6, 8)
END_DATE = datetime.now()

interesting_pages = [
    '/',
    '/contact ',
    '/meeting-squash-expert',
    '/meeting-squash-sales',
    '/free-trial',
    '/try-squash-on-demand',
    '/product-squash-tm',
    '/product-squash-autom',
    '/product-squash-devops',
    '/product-xsquash',
    '/squash-gitlab-platform',
    '/open-test-factory',
    '/pricing',
    '/community-download',
    '/download-access-tm',
    '/download-access-autom',
    '/download-access-devops',
]

# Create a new Excel workbook
workbook = openpyxl.Workbook()

def retrieve_field_stats(group, field, filter_name=None, filter_valuator=None):

    results = {}

    # Loop through months, including the current month
    current_month = START_DATE
    while current_month <= END_DATE:
        # Print the current month in YYYY-MM format
        month = current_month.strftime('%Y-%m')

        # Get the first day of the current month
        first_day = current_month.replace(day=1)
        
        # Calculate the last day of the current month
        next_month = current_month + timedelta(days=32)
        last_day = (next_month.replace(day=1) - timedelta(days=1))
        
        # Get the stats for the current month
        res = run_get_query(f"{PLAUSIBLE_API}/stats/breakdown?site_id={SITE_NAME}&property={group}:{field}&period=custom&date={first_day.strftime('%Y-%m-%d')},{last_day.strftime('%Y-%m-%d')}&metrics=visitors&limit=1000", args.token)
        results[month] = {}
        for result in res['results']:
            results[month][result[field]] = result['visitors']

        # Move to the next month
        current_month = current_month.replace(day=1) + timedelta(days=32)

    # Summarize the totals
    total = {}
    for month in results.keys():
        for source in results[month].keys():
            if source in total:
                total[source] += results[month][source]
            else:
                total[source] = results[month][source]

    # Generate the Excel sheet
    sheet = workbook.create_sheet(title=(group + '-' + field))
    row = 1
    col = 1
    if filter_name:
        col += 1
        sheet.cell(row=row, column=col, value=filter_name)
        sheet.auto_filter.ref= "B:B"
    for month in results.keys():
        col += 1
        sheet.cell(row=row, column=col, value=month)
    for source in sorted(total, key=lambda k: total[k], reverse=True):
        row += 1
        col = 1
        sheet.cell(row=row, column=col, value=source)
        if filter_name:
            col += 1
            sheet.cell(row=row, column=col, value=filter_valuator(source))
        for month in results.keys():
            col += 1
            if source in results[month]:
                sheet.cell(row=row, column=col, value=results[month][source])
            else:
                sheet.cell(row=row, column=col, value=0)

def retrieve_monthly_stats():

    # Get the monthly stats
    results = run_get_query(f"{PLAUSIBLE_API}/stats/timeseries?site_id={SITE_NAME}&period=custom&date={START_DATE.strftime('%Y-%m-%d')},{END_DATE.strftime('%Y-%m-%d')}&interval=month&metrics=visitors,pageviews,visits,views_per_visit,bounce_rate,visit_duration", args.token)

    # Generate the Excel sheet
    sheet = workbook.create_sheet(title=("monthly visitors"))
    row = 1
    sheet.cell(row=row, column=1, value='date')
    sheet.cell(row=row, column=2, value='visitors')
    sheet.cell(row=row, column=3, value='pageviews')
    sheet.cell(row=row, column=4, value='visits')
    sheet.cell(row=row, column=5, value='views_per_visit')
    sheet.cell(row=row, column=6, value='bounce_rate')
    sheet.cell(row=row, column=7, value='visit_duration')
    for result in results['results']:
        row += 1
        sheet.cell(row=row, column=1, value=result['date'])
        sheet.cell(row=row, column=2, value=result['visitors'])
        sheet.cell(row=row, column=3, value=result['pageviews'])
        sheet.cell(row=row, column=4, value=result['visits'])
        sheet.cell(row=row, column=5, value=result['views_per_visit'])
        sheet.cell(row=row, column=6, value=result['bounce_rate'])
        sheet.cell(row=row, column=7, value=result['visit_duration'])

def retrieve_daily_visitors():

    # Get the number of daily visitors
    results = run_get_query(f"{PLAUSIBLE_API}/stats/timeseries?site_id={SITE_NAME}&period=custom&date={START_DATE.strftime('%Y-%m-%d')},{END_DATE.strftime('%Y-%m-%d')}", args.token)

    # Generate the Excel sheet
    sheet = workbook.create_sheet(title=("daily visitors"))
    row = 1
    sheet.cell(row=row, column=1, value='date')
    sheet.cell(row=row, column=2, value='visitors')
    for result in results['results']:
        row += 1
        sheet.cell(row=row, column=1, value=result['date'])
        sheet.cell(row=row, column=2, value=result['visitors'])

retrieve_field_stats('visit', 'country')
retrieve_field_stats('event', 'page', 'interesting page?', lambda page: page in interesting_pages)
retrieve_field_stats('visit', 'source')
retrieve_field_stats('visit', 'utm_campaign')
retrieve_field_stats('visit', 'utm_medium')
retrieve_field_stats('visit', 'utm_source')
retrieve_monthly_stats()
retrieve_daily_visitors()

# Write the workbook
workbook.save(args.file)
