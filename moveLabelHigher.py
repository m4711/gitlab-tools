import argparse
import requests
import urllib.parse

# Command line parameters

parser = argparse.ArgumentParser()
parser.add_argument("token", help="GitLab token with API access")
parser.add_argument("group", help="group path")
parser.add_argument("name", help="name")
parser.add_argument("description", help="description")
parser.add_argument("color", help="color")
parser.add_argument("priority", help="priority")
args = parser.parse_args()




# Helpers

HEADERS = {"Authorization": "Bearer " + args.token}

def run_post_query(query):
    request = requests.post(query, headers=HEADERS)
    if request.status_code == 201:
        return
    raise Exception("Query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

def run_get_query(query):
    request = requests.get(query, headers=HEADERS)
    if request.status_code == 200:
        return request.json()
    raise Exception("Query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

def run_put_query(query, data = None):
    if data:
        request = requests.put(query, headers=HEADERS, data=data)
    else:
        request = requests.put(query, headers=HEADERS)
    if request.status_code == 200:
        return
    raise Exception("Query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

def run_delete_query(query):
    request = requests.delete(query, headers=HEADERS)
    if request.status_code in (200, 204):
        return
    raise Exception("Query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

def run_graphql_query(query):
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = HEADERS)
    if request.status_code == 200:
        return request.json()
    raise Exception("Query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))


# Data handling methods

group_id_query = """
{
  group(fullPath: "%s") {
    id
  }
}
"""

project_id_query = """
{
  project(fullPath: "%s") {
    id
  }
}
"""

group_groups_query = """
{
  group(fullPath: "%s") {
    descendantGroups(includeParentDescendants: false) {
      edges {
        node {
          fullPath
        }
      }
    }
  }
}
"""

group_projects_query = """
{
  group(fullPath: "%s") {
    descendantGroups(includeParentDescendants: false) {
      edges {
        node {
          fullPath
        }
      }
    }
    projects {
      edges {
        node {
          fullPath
        }
      }
    }
  }
}
"""

def get_group_id(groupPath):
    result = run_graphql_query(group_id_query % groupPath)
    return result["data"]["group"]["id"].rsplit('/', 1)[-1]

def get_project_id(projectPath):
    result = run_graphql_query(project_id_query % projectPath)
    return result["data"]["project"]["id"].rsplit('/', 1)[-1]

def create_group_label(groupId, groupPath, labelName, labelDescription, labelColor, labelPriority):
    params = { "name": labelName, "description": labelDescription, "color": labelColor, "priority": labelPriority}
    encodedParams = urllib.parse.urlencode(params)
    query = f"https://gitlab.com/api/v4/groups/{groupId}/labels?{encodedParams}"
    run_post_query(query)
    print(f"label '{labelName}' added on group {groupPath}", flush=True)

def get_group_label(groupId, labelName):
    query = f"https://gitlab.com/api/v4/groups/{groupId}/labels/{urllib.parse.quote(labelName, safe='')}"
    try:
        return run_get_query(query)
    except Exception as exception:
        if "Label Not Found" in str(exception):
            return None
        raise exception

def get_project_label(projectId, labelName):
    query = f"https://gitlab.com/api/v4/projects/{projectId}/labels/{urllib.parse.quote(labelName, safe='')}"
    try:
        return run_get_query(query)
    except Exception as exception:
        if "Label Not Found" in str(exception):
            return None
        raise exception

def rename_group_label(groupId, groupPath, oldLabelName, newLabelName):
    query = f"https://gitlab.com/api/v4/groups/{groupId}/labels/{urllib.parse.quote(oldLabelName, safe='')}"
    data = { "new_name": newLabelName}
    run_put_query(query, data)
    print(f"label '{oldLabelName}' renamed into '{newLabelName}' on group {groupId} ({groupPath})", flush=True)


def delete_group_label(groupId, groupPath, labelName):
    query = f"https://gitlab.com/api/v4/groups/{groupId}/labels/{urllib.parse.quote(labelName, safe='')}"
    run_delete_query(query)
    print(f"label '{labelName}' deleted on group {groupId} ({groupPath})", flush=True)

def delete_project_label(projectId, labelName):
    query = f"https://gitlab.com/api/v4/projects/{projectId}/labels/{urllib.parse.quote(labelName, safe='')}"
    run_delete_query(query)
    print(f"label '{labelName}' deleted on project {projectId}", flush=True)

def get_group_groups(groupPath):
    groups = [groupPath]
    result = run_graphql_query(group_groups_query % groupPath)
    for group_node in result["data"]["group"]["descendantGroups"]["edges"]:
        groups.extend(get_group_groups(group_node["node"]["fullPath"]))
    return groups

def get_group_projects(groupPath):
    projects = []
    result = run_graphql_query(group_projects_query % groupPath)
    for group_node in result["data"]["group"]["descendantGroups"]["edges"]:
        projects.extend(get_group_projects(group_node["node"]["fullPath"]))
    for project_node in result["data"]["group"]["projects"]["edges"]:
        projects.append(project_node["node"]["fullPath"])
    return projects

def get_group_issues_with_label(groupId, labelName):
    issues = []
    page = 1
    while True:
        query = f"https://gitlab.com/api/v4/groups/{groupId}/issues?labels={urllib.parse.quote(labelName, safe='')}&scope=all&page={page}"
        result = run_get_query(query)
        if len(result) == 0:
            return issues
        for issue in result:
            issues.append({ "projectId": issue["project_id"], "issueIid": issue["iid"]})
        page += 1

def add_label_to_issue(projectId, issueIid, labelName):
    query = f"https://gitlab.com/api/v4/projects/{projectId}/issues/{issueIid}?add_labels={labelName}"
    run_put_query(query)
    print(f"label '{labelName}' added to issue {issueIid} of project {projectId}", flush=True)



# create the new label
groupPath = args.group
newLabelName = args.name
temporaryNewLabelName = "new___" + newLabelName + "___new"
groupId = get_group_id(groupPath)
create_group_label(groupId, groupPath, temporaryNewLabelName, args.description, args.color, args.priority)
newLabelId = get_group_label(groupId, temporaryNewLabelName)["id"]

# add new label to issue already a label with the same name
for iss in get_group_issues_with_label(groupId, args.name):
    add_label_to_issue(iss["projectId"], iss["issueIid"], temporaryNewLabelName)

# delete the labels in the subgroups
for grp in get_group_groups(groupPath):
    id = get_group_id(grp)
    lb = get_group_label(id, newLabelName)
    if lb:
        delete_group_label(id, grp, newLabelName)

# delete the labels in the projects
for prj in get_group_projects(groupPath):
    id = get_project_id(prj)
    lb = get_project_label(id, newLabelName)
    if lb:
        delete_project_label(id, newLabelName)

# set the final name to the label
rename_group_label(groupId, groupPath, temporaryNewLabelName, newLabelName)


# reste à faire
# 1) paginer sur la liste des issues ayant un label