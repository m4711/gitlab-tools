# script for generating (in GitLab) the checklist for delivering a release
#
# examples of command lines (the token is a valid one, use your own):
#   python script.py Ew68sP63zNmzsFTxqJZx TM 2.1 2021-08-20
#   python script.py Ew68sP63zNmzsFTxqJZx TF 2021-09 2021-09-08


import datetime
import gitlab
import sys



# -- command line parsing and setup
if len(sys.argv) != 5:
    print(f'syntax: {sys.argv[0]} <token> <project> <release name> <release date>')
    print(f'  <project> must be "TM" or "TF"')
    print(f'  <release name> is the release name, e.g. "2.1" for TM or "2021-09" for TF')
    print(f'  <release date> must be formatted as "YYYY-MM-DD"')
    exit(1)
token = sys.argv[1]
squashProjectName = sys.argv[2]
squashReleaseName = sys.argv[3]
formattedReleaseDate = sys.argv[4]

projectID = "29117534"
try:
    gl = gitlab.Gitlab('https://gitlab.com/', private_token = token)
    gitlabProject = gl.projects.get(projectID)
except gitlab.exceptions.GitlabAuthenticationError:
    print('invalid token')
    exit(1)

if squashProjectName == 'TM':
    userPO = 'mdelobeau'
    userPFS = 'mlambrouhenix'
    userDoc = 'yboubekeur'
    preReleaseCommunicationDelay = "-60"
elif squashProjectName == 'TF':
    userPO = 'lmazure'
    userPFS = 'lmazure' # for the time being, while the AUTOM trainings are not yet rewritten, lmazure handles the training tasks
    userDoc = 'lmazure'
    preReleaseCommunicationDelay = "-30"
else:
    print('project must be "TM" or "TF"')
    exit(1)
userRecette = 'yboubekeur'
userComm = 'tlefaucheur'

try:
    releaseDate = datetime.date.fromisoformat(formattedReleaseDate)
except ValueError:
    print(f'release date must be formatted as "YYYY-MM-DD"')
    exit(1)

titlePrefix = f'[Release {squashProjectName} {squashReleaseName}]'
descriptionPostfix = f'\n\n(Cette issue a été créée par [ce script](https://gitlab.com/henixgroup/wiki-developpement-squash/-/wikis/technologie/GitLab/Script-de-cr%C3%A9ation-de-la-checklist-de-sortie-de-release).)'

# -- helper functions
def createIssue(project, title, description, assigneeId, dueDate):
    issue = project.issues.create({'title': title,
                                   'description': description,
                                   'assignee_ids': [assigneeId],
                                   'labels': ['Status::Submitted'],
                                   'due_date': dueDate.strftime('%Y-%m-%d')})
    print(f'created issue {issue.get_id()}: {title}')

def createChecklistIssue(title, description, assigneeName, delay):
    createIssue(gitlabProject,
                f'{titlePrefix} {title}',
                f'{description} {descriptionPostfix}',
                (gl.users.list(username = assigneeName)[0]).get_id(),
                releaseDate + datetime.timedelta(days = delay))

def writeDate(delay):
    d = int(delay)
    date = (releaseDate + datetime.timedelta(days = d)).strftime('%d/%m/%Y')
    if d < 0:
        return f"J-{-d} ({date})"
    if d > 0:
        return f"J+{d} ({date})"
    return f"J ({date})"

# -- generation of the checklist
createChecklistIssue(
    'Communication clients',
    '**Tâches :**\n'
    '- [ ] Communication clients post-release<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userRecette}\n'
    '\n'
    '**Description :**\n\n'
    'Deux mails différents sont envoyés pour informer les clients de la sortie de la version de Squash. Il y en a un par type d’hébergement : Serveur et SaaS. Le mail se compose d’une introduction annonçant la sortie de la version et de 5 parties :\n'
    '- Les nouveautés : présentation des grandes évolutions + liens vers l’article et la release note.\n'
    '- Le téléchargement : liens de téléchargement de la version (repo.squashtest.org + fichier avec les liens de téléchargement des plugins)\n'
    '- La documentation : lien vers la documentation de la version en français et en anglais.\n'
    '- La montée de version : rappels sur la procédure à suivre pour la montée de version + lien vers la doc et la FAQ si présente.\n'
    '- Démo et Webinar : liens vers l’instance de démo et Webinar de présentation de la version si prévu ou lien vers la page d’inscription à la newsletter et la chaîne YouTube.\n'
    'Le mail aux clients en SaaS est légèrement différent car il ne contient pas la partie téléchargement et la montée de version est réalisée par nos équipes. Le mail invite donc les clients à contacter le support afin de déterminer la date de l’intervention.\n',
    userRecette,
    0)

createChecklistIssue(
    'Communication interne',
    '**Tâches :**\n'
    '- [ ] Communication OPS pré-release<BR>\n'
    f'  Date : {writeDate(preReleaseCommunicationDelay)}<BR>\n'
    f'  Qui : @{userPO}\n'
    '- [ ] Communication manager pré release<BR>\n'
    f'  Date : {writeDate(preReleaseCommunicationDelay)}<BR>\n'
    f'  Qui : @{userPO}\n'
    '- [ ] Mail nouvelles fonctionnalités pour PFS + liens vers les US (+ démo)<BR>\n'
    f'  Date : {writeDate(-7)}<BR>\n'
    f'  Qui : @{userPO}\n'
    '- [ ] Mail communication Dev + Ops (+ EQL?)<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userPO}\n'
    '- [ ] Communication manager release<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userPO}\n'
    '\n'
    '**Description :**\n'
    '- Pour la communication managers :<BR>\n'
    'Un mail (à managers@henix.fr) informant les commerciaux de la sortie de la version est envoyé bien en amont de la release.<BR>\n'
    'Un second mail contient une liste des nouveautés, un lien vers l’article et la démo si déjà publié et divers rappels sur la communication prévue autour de la sortie de la nouvelle version.\n'
    '- Pour la communication OPS : <BR>\n'
    'Un mail (à admin@henix.fr) informant les OPS de la sortie de la version est envoyé en amont de la release.<BR>\n'
    'Un second mail contient une liste des nouveautés, un lien vers l’article, la release note, la documentation et la FAQ. Ce mail est envoyé aux développeurs des deux équipes, aux administrateurs systèmes, aux PO et au Support.\n'
    '- Pour la communication PFS :<BR>\n'
    'Un mail (à mlambrou@henix.fr pour TM, à fgautier@henix.fr pour TF) informant les PFS de la sortie de la version est envoyé en amont de la release.<BR>\n'
    'Un second mail contient une liste des nouveautés, un lien vers l’article, la release note, la documentation, la FAQ. Ce mail est envoyé aux équipes PFS.\n',
    userPO,
    0)

createChecklistIssue(
    'Communication post-release',
    '**Tâches :**\n'
    '- [ ] Relai news et liens release sur les réseaux sociaux Squash<BR>\n'
    f'  Date : {writeDate(3)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '- [ ] *Facultatif* - Relai news et lien release sur les réseaux sociaux Henix<BR>\n'
    f'  Date : {writeDate(3)}<BR>\n'
    f'  Qui :  @{userComm}\n'
    '- [ ] Intro + News + liens téléchargements + calendrier des releases dans la newsletter mensuelle<BR>\n'
    f'  Date : {writeDate(15)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '\n'
    '**Description :**\n\n'
    'Une fois la release annoncée et publiée sur le site squashtest.com à travers une news, cette dernière peut être relayée via :\n'
    '- la newsletter mensuelle Squash (FR et EN)\n'
    '- les réseaux sociaux Squash \n'
    '- les réseaux sociaux Henix (facultatif)\n'
    '- le site henix.com (facultatif)\n'
    '- l’intranet Henix (facultatif)\n',
    userComm,
    15)

createChecklistIssue(
    'Mise à disposition des packages',
    '**Tâches :**\n'
    '- [ ] Mettre à jour doc des compatibilités + plugins<BR>\n'
    f'  Date : {writeDate(-7)}<BR>\n'
    '  Qui : @elebouvier\n'
    '- [ ] Publication des packages sur le repo<BR>\n'
    f'  Date : {writeDate(-2)}<BR>\n'
    f'  Qui : @{userPO}\n' +
    ('- [ ] Mise à jour de l’image Docker de Squash TM<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    '  Qui : @kungnx\n' if (squashProjectName == 'TM') else '') +
    '- [ ] Dépôt des plugins payants sur le Drive<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userRecette}\n' +
    ('- [ ] Publication des sources sur le repo public<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userPO}\n' if (squashProjectName == 'TM') else '') +
    '\n'
    '**Description :**\n'
    '- Pour la version Communauté :<BR>\n'
    'Cette mise à disposition se fait après validation et recette de la version finale de la nouvelle version. Elle concerne tous les liens de téléchargement des composants (inclus dans la licence Community) de la release.<BR>\n'
    'Nous considérons qu’une release est prête à être communiquée à partir du moment où l’image Docker de cette version est publiée.\n'
    '- Pour la version Premium :<BR>\n'
    'Un dossier Drive partagé permet au Support Squash de partager aux clients les plugins licenciés. Il faut créer un nouveau dossier ("VERSION x.xx") pour la version. A la racine de ce dossier, il faut ajouter un dossier pour chaque plugin en reprenant l’appellation de ces derniers dans la grille tarifaire. Il faudra ensuite ajouter la dernière version compressée des plugins (.zip et .tar.gz) dans leur dossier respectif. Un document Google Sheet est complété pour rassembler les liens de téléchargement des plugins compatibles avec la version.\n',
    userRecette,
    0)

createChecklistIssue(
    'Mise à jour des formations',
    '**Tâches :**\n'
    '- [ ] Mise à jour des formations<BR>\n'
    f'  Date : {writeDate(21)}<BR>\n'
    f'  Qui : @{userPFS}\n'
    '\n'
    '**Description :**\n\n'
    'Formations concernées :\n'
    '- TM1\n'
    '- TM2\n'
    '- PIL1\n'
    '- mais il y en a d’autres, à mettre à jour par les formateurs de l’EQL et consorts\n\n'
    'dans les formats Opale, PPT pour Moodle.\n\n'
    'Mettre à jour également les JDD et les instances.\n',
    userPFS,
    21)

createChecklistIssue(
    'Mise à jour des instances de démo / formation',
    '**Tâches :**\n' +
    ('- [ ] Mettre à jour les instances démos en ligne sur le site FR et EN<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userPO}\n' if (squashProjectName == 'TM') else '') +
    '- [ ] Mettre à jour l’instance démo manager<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userPO}\n' +
    ('- [ ] Mettre à jour les instances de formation<BR>\n'
    f'  Date : {writeDate(10)}<BR>\n'
    f'  Qui : @{userPO}\n' if (squashProjectName == 'TM') else '') +
    '\n'
    '**Description :**\n' +
    ('- Pour l’instance de démo en ligne :<BR>\n'
    'La démo de Squash TM présente sur le site internet doit être mise à jour avec la nouvelle version de l’application.<BR>\n'
    'Cette activité fait l’objet d’un ticket sur la Crêperie à destination des administrateurs systèmes.\n' if (squashProjectName == 'TM') else '') +
    '- Pour l’instance de démo manager :<BR>\n'
    'La démo de Squash disponible pour les besoins de démos des managers doit être mise à jour avec la nouvelle version de l’application.<BR>\n'
    'Cette activité fait l’objet d’un ticket sur la Crêperie à destination des administrateurs systèmes.<BR>\n' +
    ('- Pour les instances de démo formation :<BR>\n'
    'Les instances de Squash disponibles pour les formations doivent être mose à jour avec la nouvelle version de l’application.\n'
    'Cette activité fait l’objet d’un ticket sur la Crêperie à destination des administrateurs systèmes.\n' if (squashProjectName == 'TM') else '') +
    ('- Plus la mise à jour :\n'
    '    - du script de présentation de Squash,\n'
    '    - des jeux de données éventuellement.\n' if (squashProjectName == 'TM') else ''),
    userPO,
    10)

createChecklistIssue(
    'Mise à jour du site squashtest.com',
    '**Tâches :**\n'
    '- [ ] Publication liens téléchargements release + plugins sur le site FR et EN<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '- [ ] *Facultatif* - Publication liens téléchargements release Squash TM Legacy sur le site FR et EN<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '- [ ] Publication mise à jour page Roadmap & releases<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '- [ ] Publication mise à jour page Roadmap & releases du ou des modules concernés<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '- [ ] Publication mise à jour page Historique des versions du ou des modules concernés<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '- [ ] *Facultatif* - Publication mise à jour captures release sur le site<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '- [ ] Publication news release sur le site FR et EN<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '- [ ] Référencement de la news release + référencement images + définition URL<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '\n'
    '**Description :**\n'
    '- Pour le site :<BR>\n'
    'Cette mise à jour se fait après validation et recette de la version finale de la nouvelle version. Elle concerne tous les liens de téléchargement des composants (inclus dans la licence Community) de la release.<BR>\n'
    'La dernière version de Squash est publiée sur http://repo.squashtest.com/ par l’équipe de développeurs. L’équipe Communication est chargée de mettre à jour les liens de téléchargement de l’application en ligne sur les versions FR et EN du site.<BR>\n'
    'Plus la mise à jour FR et EN :\n'
    '    - de l’ensemble des pages Communautés liées à la release (Roadmap & Releases, Roadmap & Releases Squash XX, Historique des versions Squash XX) selon les demandes des PO.\n'
    '    - de captures d’écran illustratives si impactées par la release\n'
    '    - du contenu de la liste des fonctionnalités si impacté par la release\n'
    '    - de tout élément des pages du site si impacté par la release\n'
    '\n'
    '- Pour la section news du site :<BR>\n'
    'Un article illustré par des captures d’écran est à rédiger en français et en anglais pour être publié sur le site squashtest.com pour présenter les évolutions de la nouvelle version.\n',
    userComm,
    0)

createChecklistIssue(
    'Mise à jour du kit de vente',
    '**Tâches :**\n'
    '- [ ] Mise à jour du kit de vente<BR>\n'
    f'  Date : {writeDate(-7)}<BR>\n'
    f'  Qui : @{userPO}\n'
    '\n'
    '**Description :**\n\n'
    'Quelques transparents à destination des commerciaux, pour résumer et présenter la release note commercialement.',
    userPO,
    -7)

createChecklistIssue(
    'Mise à jour release note / documentation',
    '**Tâches :**\n'
    '- [ ] Publication releases notes dans la doc FR et EN<BR>\n'
    f'  Date : {writeDate(-7)}<BR>\n'
    f'  Qui : @{userDoc}\n'
    '- [ ] *Facultatif* - Réalisation captures écran / schémas FR et EN pour màj de la doc<BR>\n'
    f'  Date : {writeDate(-7)}<BR>\n'
    f'  Qui : @{userDoc}\n'
    '- [ ] Rédaction mise à jour documentation (+ FAQ si besoin) FR et EN<BR>\n'
    f'  Date : {writeDate(-7)}<BR>\n'
    f'  Qui : @{userPO}\n'
    '- [ ] Publication mise à jour documentation + images (+ FAQ si besoin) FR et EN<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userDoc}\n' +
    ('- [ ] Mise à jour de la doc d’OpenTestFactory<BR>\n'
    f'  Date : {writeDate(0)}<BR>\n'
    f'  Qui : @{userPO}\n' if (squashProjectName == 'TF') else '') +
    '\n'
    '**Description :**\n'
    '- Pour la documentation :<BR>\n'
    'Cette mise à jour se fait au fur et à mesure du développement des évolutions. Un ticket Jira pour la rédaction de la documentation est créé dans le projet « Squash DOC » pour chaque User Story du projet « Squash Story Mapping » qui nécessite une documentation.<BR>\n'
    'En fonction de la fonctionnalité, il faudra mettre à jour l’un de 3 guides de référence (Guides d’installation, utilisateur ou administrateur). Il faudra y intégrer la description et les captures d’écran des nouvelles     \n''fonctionnalités de l’application.<BR>\n'
    'La documentation de Squash est stockée sur Gitlab. Il faut un compte Gitlab et être ajouté par un administrateur pour y contribuer. Nous travaillons pour l’instant sur une seule branche, il faut donc être vigilant lors de la rédaction à ne pas travailler sur le même fichier .md qu’un autre collègue. Nous travaillons avec un clone du repo Gitlab en local avant de pousser nos modifications sur le dépôt distant. Un build est programmé toutes les heures pour mettre en ligne la documentation sur le site à disposition du public.\n'
    '- Pour la release note :<BR>\n'
    'La Release note est un document technique qui liste les informations de la version qu’elle accompagne. <BR>\n'
    'Pour faciliter la conception de la Release note, il est conseillé de créer deux filtres dans Jira pour avoir d’un côté les évolutions et de l’autre les anomalies de la version.<BR>\n'
    'La Release note de Squash se compose de deux parties :\n'
    '    - La première partie liste les évolutions développées pour la version. Les évolutions sont triées par grands espaces pour faciliter la lecture.\n'
    '    - La deuxième partie liste les anomalies corrigées dans la version (Impacte la production : Oui). Les anomalies issues des développements de la version ne doivent pas apparaitre dans cette liste (Impacte la production : Non). Les corrections sont également triées par grands espaces. Les anomalies déclarées dans Mantis doivent afficher leur ID.\n'
    'La Release note est incluse sur le site internet de la documentation. Elle est présente en français et en anglais. Si les release notes partagent la même version majeure, elles sont mises à la suite dans la même page sinon une nouvelle la release note est créé dans la nouvelle version de la doc.\n'
    '- Pour la FAQ (facultatif) :<BR>\n'
    'Une FAQ est sur le site de la documentation pour déblayer les questions récurrentes sur la future version. Elle peut être mise à jour en fonction des fonctionnalités ajoutées par la release.\n',
    userPO,
    0)

createChecklistIssue(
    'Webinar',
    '**Tâches :**\n'
    '- [ ] *Facultatif* - Webinar Présentation release<BR>\n'
    f'  Date : {writeDate(30)}<BR>\n'
    f'  Qui : @{userPO} @{userComm}\n'
    '- [ ] *Facultatif* - Montage / sous titrage / publication replay Webinar release<BR>\n'
    f'  Date : {writeDate(60)}<BR>\n'
    f'  Qui : @{userComm}\n'
    '\n'
    '**Description :**<BR>\n'
    'Quand la release le nécessite ou le permet, un webinar dédié à une nouvelle version de Squash TM ou AUTOM/DEVOPS peut être organisé avec un PO, une personne du Support ou des PFS.\n',
    userComm,
    60)
