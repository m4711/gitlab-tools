#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "Syntax: $0 <project ID> <access token>"
    exit 2
fi

group_id=$1
access_token=$2

create_label() {
    curl --data-urlencode name="$1" \
         --data-urlencode description="$2" \
         --data-urlencode color="$3" \
         --data-urlencode priority="$4" \
         --header "PRIVATE-TOKEN: $access_token" \
         "https://gitlab.com/api/v4/groups/$group_id/labels"
}

# ------------------------------------------------------------------------------------------------
# labels for implementation groups (uncomment the following lines if applicable)

create_label "StatusE::New"              "Epic not ready yet"                   "#009966" "2001"
create_label "StatusE::Backlog"          "Epic ready"                           "#009966" "2002"
create_label "StatusE::InProgress"       "Epic in progress"                     "#009966" "2003"
create_label "StatusE::Done"             "Epic finished"                        "#009966" "2004"
create_label "StatusE::Suspended"        "Epic suspended"                       "#009966" "2005"
create_label "StatusE::Cancelled"        "Epic cancelled"                       "#009966" "2006"
