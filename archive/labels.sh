#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "Syntax: $0 <group ID> <access token>"
    exit 2
fi

group_id=$1
access_token=$2

create_label() {
    curl --data-urlencode name="$1" \
         --data-urlencode description="$2" \
         --data-urlencode color="$3" \
         --data-urlencode priority="$4" \
         --header "PRIVATE-TOKEN: $access_token" \
         "https://gitlab.com/api/v4/groups/$group_id/labels"
}


create_label "Type::UserStory"           "User story"                            "#0000ff"
create_label "Type::Bug"                 "Bug"                                   "#0000ff"
create_label "Type::Task"                "Task"                                  "#0000ff"
create_label "Type::Study"               "Study"                                 "#0000ff"
create_label "Type::ToBeRefined"         ""                                      "#0000ff"

create_label "Status::Backlog"          ""         "#6699cc"
create_label "Status::Cancelled"        ""         "#6699cc"
create_label "Status::Draft"            ""         "#6699cc"
create_label "Status::Feedback"         ""         "#6699cc"
create_label "Status::Implemented"      ""         "#6699cc"
create_label "Status::InProgress"       ""         "#6699cc"
create_label "Status::SmokeTested"      ""         "#6699cc"
create_label "Status::Suspended"        ""         "#6699cc"
create_label "Status::ToBeCorrected"    ""         "#6699cc"
create_label "Status::Validated"        ""         "#6699cc"

create_label "ProdImpact::Yes"           "Exists in production"                  "#9400d3"
create_label "ProdImpact::No"            "Does not exist in production"          "#9400d3"
create_label "ProdImpact::ToBeAnalyzed"  "Impact on production to be analyzed"   "#9400d3"

create_label "DemoPO::Ko"       ""   "#ff4466"
create_label "DemoPO::Ok"       ""   "#ff4488"
create_label "DemoPO::Waiting"  ""   "#ff44aa"

create_label "Release::7.0.0"  ""   "#bbbbff"
create_label "Release::8.0.0"  ""   "#bbbbff"
create_label "Release::9.0.0"  ""   "#bbbbff"
create_label "Release::10.0.0" ""   "#bbbbff"
create_label "Release::2024-03" ""   "#bbbbff"
create_label "Release::2024-04" ""   "#bbbbff"
create_label "Release::2024-05" ""   "#bbbbff"
create_label "Release::2024-06" ""   "#bbbbff"
create_label "Release::2024-07" ""   "#bbbbff"
create_label "Release::2024-09" ""   "#bbbbff"
create_label "Release::2024-10" ""   "#bbbbff"
create_label "Release::2024-11" ""   "#bbbbff"
create_label "Release::2024-12" ""   "#bbbbff"

create_label "Priority::Highest"  "Highest priority"   "#ff0000"
create_label "Priority::High"     "High priority"      "#ff4400"
create_label "Priority::Medium"   "Medium priority"    "#ff8800"
create_label "Priority::Low"      "Low priority"       "#ffbb00"
create_label "Priority::Lowest"   "Lowest priority"    "#ffe000"

create_label "Deployment"   "Indicates that the issue has an impact on the deployment"    "#664499"

create_label "Team::AI"            "Epic/issue owned by the AI team"                "#00ffff"
create_label "Team::Agile"         "Epic/issue owned by the Agile Tester team"      "#00ffff"
create_label "Team::Automated"     "Epic/issue owned by the Automated Test team"    "#00ffff"
create_label "Team::Forge"         "Epic/issue owned by the DevOps team"            "#00ffff"
create_label "Team::Other"         "Epic/issue owned by a non feature team"         "#00ffff"
create_label "Team::Performance"   "Epic/issue owned by the Performance team"       "#00ffff"
create_label "Team::Portal"        "Epic/issue owned by the SquashPortal team"      "#00ffff"
