import argparse
import requests
import urllib.parse

# Command line parameters
parser = argparse.ArgumentParser()
parser.add_argument("token", help="GitLab token with API access")
parser.add_argument("name", help="name")
parser.add_argument("description", help="description")
parser.add_argument("color", help="color")
parser.add_argument("priority", help="priority")
args = parser.parse_args()


HEADERS = {"Authorization": "Bearer " + args.token}

# Helpers
def run_query(query):
    request = requests.post(
        query, headers=HEADERS
    )
    if request.status_code == 201:
        return request.json()
    print("Query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

# Projects and groups in which to create the label
projectIds = [
]

groupIds = [
    15076073,   # https://gitlab.com/henixdevelopment/squash
    52381749,   # https://gitlab.com/henixdevelopment/open-source
    62438893,   # https://gitlab.com/henixdevelopment/test/test-end-to-end
    15985639,   # https://gitlab.com/henixdevelopment/forge-squash
    64488456,   # https://gitlab.com/henixdevelopment/platform-devops
]

# Create the label
params = { "name": args.name, "description": args.description, "color": args.color, "priority": args.priority}
encodedParams = urllib.parse.urlencode(params)

for id in projectIds:
    query = f"https://gitlab.com/api/v4/projects/{id}/labels?{encodedParams}"
    run_query(query)

for id in groupIds:
    query = f"https://gitlab.com/api/v4/groups/{id}/labels?{encodedParams}"
    run_query(query)
