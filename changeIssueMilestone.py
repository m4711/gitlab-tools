import argparse
import requests
import urllib

# groups to be checked
squash_top_groups = [
    "henixdevelopment/open-source",
    "henixdevelopment/forge-squash",
    "henixdevelopment/platform-devops",
    "henixdevelopment/squash",
    "henixdevelopment/test/test-end-to-end"
]

parser = argparse.ArgumentParser()
parser.add_argument("token", help="GitLab token with API access")
parser.add_argument("oldMilestone", help="old milestone")
parser.add_argument("newMilestone", help="new milestone")
parser.add_argument("filter", help="filter", nargs='?')
args = parser.parse_args()


# ----------------------------------
# Request helpers
# ----------------------------------

def run_graphql_query(query, token):
    header = {"Authorization": "Bearer " + token}
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = header)
    if request.status_code == 200:
        return request.json()
    raise Exception(f"Query failed to run by returning code of {request.status_code}. {query}")

def run_get_query(query, token):
    header = {"Authorization": "Bearer " + token}
    request = requests.get(query, headers = header)
    if request.status_code == 200:
        return request.json()
    raise Exception(f"GET Query {query} failed to run by returning code of {request.status_code}. {query}")

def run_put_query(query, token, data = None):
    header = {"Authorization": "Bearer " + token}
    if data:
        request = requests.put(query, headers=header, data=data)
    else:
        request = requests.put(query, headers=header)
    if request.status_code == 200:
        return
    raise Exception(f"PUT Query {query} failed to run by returning code of {request.status_code}. {query}")


# ----------------------------------
# Issue helpers
# ----------------------------------

def get_issue_data(project, iid, token):
    query = f"https://gitlab.com/api/v4/projects/{urllib.parse.quote(project, safe='')}/issues?iids[]={iid}"
    result = run_get_query(query, token)
    issue_data = result[0]
    issue = { "title":     issue_data["title"],
              "iid":       iid}
    return issue

# ----------------------------------
# Project/group helpers
# ----------------------------------

issues_query = """
{
  project(fullPath: "%s") {
    issues(types: [ISSUE], milestoneTitle: "%s"%s, state: opened, after: "%s") {
      edges {
        cursor
        node {
          iid
          title
        }
      }
    }
  }
}
"""

subproject_query = """
{
  group(fullPath: "%s") {
    projects (after: "%s") {
      edges {
        cursor
        node {
          fullPath
        }
      }
    }
  }
}
"""
subgroup_query = """
{
  group(fullPath: "%s") {
    descendantGroups(includeParentDescendants: false, after: "%s")  {
      edges {
        cursor
        node {
          fullPath
        }
      }
    }
  }
}
"""

# list of all groups (directly of at any depth) below a group
def get_group_groups(group, token):
    groups = [ group ]
    cursor = ""
    while True:
        result = run_graphql_query(subgroup_query % (group, cursor), token)
        if len(result["data"]["group"]["descendantGroups"]["edges"]) == 0:
            return groups
        for group_node in result["data"]["group"]["descendantGroups"]["edges"]:
            groups.extend(get_group_groups(group_node["node"]["fullPath"], token))
            cursor = group_node["cursor"]

# list of all projects directly below a group
def get_group_projects(group, token):
    projects = []
    cursor = ""
    while True:
        result = run_graphql_query(subproject_query % (group, cursor), token)
        if len(result["data"]["group"]["projects"]["edges"]) == 0:
            return projects
        for project_node in result["data"]["group"]["projects"]["edges"]:
            projects.append(project_node["node"]["fullPath"])
            cursor = project_node["cursor"]

# list of all groups in Squash code base
def get_squash_groups(token):
    groups = []
    for group in squash_top_groups:
        groups.extend(get_group_groups(group, token))
    return groups

# list of all projects in Squash code base
def get_squash_projects(token):
    projects = []
    for group in get_squash_groups(token):
        projects.extend(get_group_projects(group, token))
    return projects

# ----------------------------------
# We perform the work here…
# ----------------------------------

def get_milestone_id(project, milestoneName, token):
    query = f"https://gitlab.com/api/v4/projects/{urllib.parse.quote(project, safe='')}/milestones?include_parent_milestones=true&title={urllib.parse.quote(milestoneName, safe='')}"
    result = run_get_query(query, token)
    return result[0]["id"]

def set_issue_milestone(project, issueIid, milestoneId, token):
    query = f"https://gitlab.com/api/v4/projects/{urllib.parse.quote(project, safe='')}/issues/{issueIid}?milestone_id={milestoneId}"
    run_put_query(query, token)

def handle_project_issues(project, filter, oldMilestone, newMilestone, token):
    newMilestoneId = 0
    cursor = ""
    if (filter):
        filter = ", " + filter
    else:
        filter = ""
    while True:
        issue_result = run_graphql_query(issues_query % (project, oldMilestone, filter, cursor), token)
        if issue_result["data"]["project"] is None:
            print(f"Project {project} is empty", flush=True)
            continue
        if len(issue_result["data"]["project"]["issues"]["edges"]) == 0:
            break
        for issue in issue_result["data"]["project"]["issues"]["edges"]:
            if newMilestoneId == 0:
                newMilestoneId = get_milestone_id(project, newMilestone, token)
            node = issue["node"]
            if not node:
                print(f"Failed to retrieve an issue of project {project} (this is a GitLab bug)", flush=True)
                return False
            print(f"{node['iid']} {node['title']}", flush=True)
            set_issue_milestone(project, node['iid'], newMilestoneId, token)
            cursor = issue["cursor"]

def handle_projects(projects, filter, oldMilestone, newMilestone, token):
    for project in projects:
        print(f">>> Project {project}", flush=True)
        handle_project_issues(project, filter, oldMilestone, newMilestone, token)

# ----------------------------------
# Main
# ----------------------------------

projects = get_squash_projects(args.token)
handle_projects(projects, args.filter, args.oldMilestone, args.newMilestone, args.token)
