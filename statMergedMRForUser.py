import requests
from datetime import datetime, timedelta
from collections import defaultdict
import argparse
import sys

def get_weekly_merged_mrs(private_token, username):
    headers = {
        'Private-Token': private_token
    }
    
    # Initialize variables
    page = 1
    per_page = 100
    weekly_counts = defaultdict(int)
    weekly_mrs = dict()
    
    while True:
        # Construct API URL for merge requests
        url = "https://gitlab.com/api/v4/merge_requests"
        
        params = {
            'state': 'merged',
            'merge_user_username': username,
            'scope': 'all',
            'page': page,
            'per_page': per_page,
            'order_by': 'updated_at',
            'sort': 'desc'
        }
        
        # Make API request
        print("+", flush=True, end="")
        response = requests.get(url, headers=headers, params=params)
        response.raise_for_status()
        
        mrs = response.json()
        if not mrs:
            break
            
        # Process merge requests
        for mr in mrs:
            if mr['merged_at']:
                merged_date = datetime.strptime(mr['merged_at'], '%Y-%m-%dT%H:%M:%S.%fZ')
                # Get the Monday of the week
                monday = (merged_date - timedelta(days=merged_date.weekday())).date()
                weekly_counts[monday] += 1
                if monday not in weekly_mrs.keys():
                    weekly_mrs[monday] = []
                weekly_mrs[monday].append(f"{mr['web_url']} {mr['title']}")
        
        # Check if we've processed all pages
        if len(mrs) < per_page:
            break

        page += 1

    print("\n")

    return dict(sorted(weekly_mrs.items()))

def format_weekly_report(weekly_counts):

    report = "Weekly Merged MRs Report\n"
    report += "=====================\n\n"
    
    for week, mrs in weekly_counts.items():
        week_end = week + timedelta(days=6)
        report += f"Week of {week.strftime('%Y-%m-%d')} to {week_end.strftime('%Y-%m-%d')}: {len(mrs)} MRs\n"
        for mr in mrs:
            report += f"{mr}\n"
    return report

def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(
        description='Analyze weekly merged GitLab merge requests for a specific user.'
    )
    
    # Add positional arguments
    parser.add_argument('token', help='GitLab private access token')
    parser.add_argument('username', help='GitLab username to analyze')
    
    # Parse arguments
    args = parser.parse_args()
    
    try:
        weekly_mrs = get_weekly_merged_mrs(args.token, args.username)
        report = format_weekly_report(weekly_mrs)
        print(report)
        
    except requests.exceptions.RequestException as e:
        print(f"Error accessing GitLab API: {e}", file=sys.stderr)
        sys.exit(1)
    except Exception as e:
        print(f"An error occurred: {e}", file=sys.stderr)
        sys.exit(1)

if __name__ == "__main__":
    main()