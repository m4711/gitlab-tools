import argparse
import requests
import urllib.parse

# Command line parameters

parser = argparse.ArgumentParser()
parser.add_argument("token", help="GitLab token with API access")
parser.add_argument("group", help="group path")
parser.add_argument("name", help="name")
args = parser.parse_args()


# Helpers

HEADERS = {"Authorization": "Bearer " + args.token}

def run_get_query(query):
    request = requests.get(query, headers=HEADERS)
    if request.status_code == 200:
        return request.json()
    raise Exception("Query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

def run_graphql_query(query):
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = HEADERS)
    if request.status_code == 200:
        return request.json()
    raise Exception("Query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))


# Data handling methods

group_id_query = """
{
  group(fullPath: "%s") {
    id
  }
}
"""

project_id_query = """
{
  project(fullPath: "%s") {
    id
  }
}
"""

group_groups_query = """
{
  group(fullPath: "%s") {
    descendantGroups(includeParentDescendants: false) {
      edges {
        node {
          fullPath
        }
      }
    }
  }
}
"""

group_projects_query = """
{
  group(fullPath: "%s") {
    descendantGroups(includeParentDescendants: false) {
      edges {
        node {
          fullPath
        }
      }
    }
    projects {
      edges {
        node {
          fullPath
        }
      }
    }
  }
}
"""

def get_group_id(groupPath):
    result = run_graphql_query(group_id_query % groupPath)
    return result["data"]["group"]["id"].rsplit('/', 1)[-1]

def get_project_id(projectPath):
    result = run_graphql_query(project_id_query % projectPath)
    return result["data"]["project"]["id"].rsplit('/', 1)[-1]

def get_group_label(groupId, labelName):
    query = f"https://gitlab.com/api/v4/groups/{groupId}/labels/{urllib.parse.quote(labelName, safe='')}?include_ancestor_groups=false"
    try:
        return run_get_query(query)
    except Exception as exception:
        if "Label Not Found" in str(exception):
            return None
        raise exception

def get_project_label(projectId, labelName):
    query = f"https://gitlab.com/api/v4/projects/{projectId}/labels/{urllib.parse.quote(labelName, safe='')}?include_ancestor_groups=false"
    try:
        return run_get_query(query)
    except Exception as exception:
        if "Label Not Found" in str(exception):
            return None
        raise exception

def get_group_groups(groupPath):
    groups = [groupPath]
    result = run_graphql_query(group_groups_query % groupPath)
    for group_node in result["data"]["group"]["descendantGroups"]["edges"]:
        groups.extend(get_group_groups(group_node["node"]["fullPath"]))
    return groups

def get_group_projects(groupPath):
    projects = []
    result = run_graphql_query(group_projects_query % groupPath)
    for group_node in result["data"]["group"]["descendantGroups"]["edges"]:
        projects.extend(get_group_projects(group_node["node"]["fullPath"]))
    for project_node in result["data"]["group"]["projects"]["edges"]:
        projects.append(project_node["node"]["fullPath"])
    return projects

groupPath = args.group
newLabelName = args.name

for grp in get_group_groups(groupPath):
    id = get_group_id(grp)
    lb = get_group_label(id, newLabelName)
    if lb:
        print(f"group {grp}", flush=True)

for prj in get_group_projects(groupPath):
    id = get_project_id(prj)
    lb = get_project_label(id, newLabelName)
    if lb:
        print(f"project {prj}", flush=True)
