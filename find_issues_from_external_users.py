import json
import os
import urllib.request
import urllib.parse
import urllib.error
from typing import List, Dict, Any

def get_gitlab_auth_headers() -> Dict[str, str]:
    token = os.getenv('GITLAB_TOKEN')
    if not token:
        raise ValueError("GITLAB_TOKEN environment variable not set")
    return {
        'PRIVATE-TOKEN': token,
        'Content-Type': 'application/json'
    }

def make_gitlab_request(url: str, headers: Dict[str, str]) -> Any:
    try:
        request = urllib.request.Request(url, headers=headers)
        with urllib.request.urlopen(request) as response:
            return json.loads(response.read().decode())
    except urllib.error.HTTPError as e:
        print(f"Error making request to {url}: {e}")
        print(f"Response message: {e.read().decode()}")  # Add error response details
        return None
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON response from {url}: {e}")
        return None

def get_project_id(headers: Dict[str, str], project_path: str) -> int:
    encoded_path = urllib.parse.quote(project_path, safe='')
    url = f"https://gitlab.com/api/v4/projects/{encoded_path}"

    response = make_gitlab_request(url, headers)
    if not response:
        raise ValueError(f"Could not find project: {project_path}")

    return response['id']

def get_file_content(headers: Dict[str, str], project_path: str, file_path: str) -> Dict[str, Any]:
    # First get the project ID
    project_id = get_project_id(headers, project_path)

    # URL encode the file path - remove leading slash if present
    file_path = file_path.lstrip('/')
    encoded_file_path = urllib.parse.quote(file_path, safe='')
    url = f"https://gitlab.com/api/v4/projects/{project_id}/repository/files/{encoded_file_path}/raw?ref=main"

    content = make_gitlab_request(url, headers)
    if not content:
        raise ValueError(f"Could not fetch configuration file at {file_path}")

    return content

def get_all_projects(headers: Dict[str, str], group_path: str) -> List[Dict[str, Any]]:
    projects = []
    page = 1

    while True:
        encoded_path = urllib.parse.quote(group_path, safe='')
        url = f"https://gitlab.com/api/v4/groups/{encoded_path}/projects?include_subgroups=true&page={page}&per_page=100"
        response = make_gitlab_request(url, headers)

        if not response or len(response) == 0:
            break

        projects.extend(response)
        page += 1

    return projects

def get_project_issues(headers: Dict[str, str], project_id: int) -> List[Dict[str, Any]]:
    issues = []
    page = 1

    while True:
        url = f"https://gitlab.com/api/v4/projects/{project_id}/issues?page={page}&per_page=100"
        response = make_gitlab_request(url, headers)

        if not response or len(response) == 0:
            break

        issues.extend(response)
        page += 1

    return issues

def main():
    try:
        headers = get_gitlab_auth_headers()

        # Get configuration file and extract GitLab IDs
        config = get_file_content(
            headers,
            "henixdevelopment/sandbox/lmz/gtr",
            "src/main/resources/conf.json"
        )
        known_user_names = set(user['gitlabId'] for user in config.get('users', []))
        known_user_names.add("henix-gitlab-bot")

        # Get all projects
        projects = get_all_projects(headers, "henixdevelopment/open-source")

        # Get and filter issues
        for project in projects:
            print(f"\nChecking issues for project: {project['path_with_namespace']}")
            issues = get_project_issues(headers, project['id'])

            filtered_issues = [
                issue for issue in issues
                if issue['author']['username'] not in known_user_names
            ]

            if filtered_issues:
                print(f"Found {len(filtered_issues)} issues from unknown users:")
                for issue in filtered_issues:
                    print(f"\nURL: {issue['web_url']}")
                    print(f"State: {issue['state']}")
                    print(f"Title: {issue['title']}")
                    print(f"Author: {issue['author']['username']} ({issue['author']['name']})")
                    print(f"Labels: {', '.join(issue['labels']) if issue['labels'] else 'No labels'}")

    except ValueError as e:
        print(f"Configuration error: {e}")
    except urllib.error.URLError as e:
        print(f"Network error: {e}")
    except KeyError as e:
        print(f"Data format error: Missing required field {e}")
    except Exception as e:
        print(f"Unexpected error occurred: {e}")
        raise  # Re-raise unexpected exceptions for debugging

if __name__ == "__main__":
    main()