import os
import requests
import sys
import tempfile

if len(sys.argv) != 2:
    print(f'syntax: {sys.argv[0]} <token>')
    exit(1)
token = sys.argv[1]

HEADERS = {"Authorization": "Bearer " + token}

def run_query(query):
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = HEADERS)
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception(f"Query failed to run by returning code of {request.status_code}. {query}")

all_users = set()

project_list_query = """
{
  group(fullPath: "henixdevelopment/squash/squash-tm/core") {
    projects (includeSubgroups: true) {
      edges {
        node {
          fullPath
        }
      }
    }
  }
}
"""

branch_list_query = """
{
  project(fullPath: "%s") {
    repository {
      branchNames(searchPattern: "*", offset:0, limit: 200)
    }
  }
}
"""

report = tempfile.NamedTemporaryFile(mode="w", suffix=".html", delete=False)

report.write("<!DOCTYPE html>\n<html>\n<head>\n<meta charset='utf-8'>\n</head>\n<body>\n")

result = run_query(project_list_query)
for project_node in result["data"]["group"]["projects"]["edges"]:
    project_full_path = project_node["node"]["fullPath"]
    report.write(project_full_path + "<ul>")
    resultb = run_query(branch_list_query % project_full_path)
    for branch in resultb["data"]["project"]["repository"]["branchNames"]:
        report.write("<li>" + branch + "<img src='https://gitlab.com/" +project_full_path  + "/badges/" + branch + "/pipeline.svg'> </li>")
    report.write("</ul>")

report.write("</body>\n</html>")

os.startfile(report.name)
