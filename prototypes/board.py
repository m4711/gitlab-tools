import gitlab
import sys

# -- helper functions
labels = None
def getLabelId(labelName):
    global labels
    if labels is None:
        labels = gitlabGroup.labels.list(all=True)
    return list(filter(lambda l: l.name == labelName, labels))[0].id

# -- command line parsing and setup
if len(sys.argv) != 3:
    print(f'syntax: {sys.argv[0]} <token> <project ID>')
    exit(1)
token = sys.argv[1]
groupID = sys.argv[2]
try:
    gl = gitlab.Gitlab('https://gitlab.com/', private_token = token)
    gitlabGroup = gl.groups.get(groupID)
except gitlab.exceptions.GitlabAuthenticationError:
    print('invalid token')
    exit(1)
except gitlab.exceptions.GitlabGetError:
    print('invalid project ID')
    exit(1)

def create_user_story_board(title, labels = None):
    board = gitlabGroup.boards.create({'name': title})
    l = 'Type::UserStory'
    if labels:
        l = l + "," +  ",".join(labels)
    board.labels = l
    board.save()
    board.lists.create({'label_id': getLabelId('StatusU::New')})
    board.lists.create({'label_id': getLabelId('StatusU::Backlog')})
    board.lists.create({'label_id': getLabelId('StatusU::ImplemInProgress')})
    board.lists.create({'label_id': getLabelId('StatusU::ReadyForTest')})
    board.lists.create({'label_id': getLabelId('StatusU::SmokeTested')})
    board.lists.create({'label_id': getLabelId('StatusU::ToBeCorrected')})
    board.lists.create({'label_id': getLabelId('StatusU::Validated')})
    board.lists.create({'label_id': getLabelId('StatusU::Suspended')})
    board.lists.create({'label_id': getLabelId('StatusU::Cancelled')})

def create_bug_board(title, labels = None):
    board = gitlabGroup.boards.create({'name': title})
    l = 'Type::Bug'
    if labels:
        l = l + "," +  ",".join(labels)
    board.labels = l
    board.save()
    board.lists.create({'label_id': getLabelId('StatusB::New')})
    board.lists.create({'label_id': getLabelId('StatusB::Backlog')})
    board.lists.create({'label_id': getLabelId('StatusB::FixInProgress')})
    board.lists.create({'label_id': getLabelId('StatusB::ReadyForTest')})
    board.lists.create({'label_id': getLabelId('StatusB::SmokeTested')})
    board.lists.create({'label_id': getLabelId('StatusB::TestKo')})
    board.lists.create({'label_id': getLabelId('StatusB::Validated')})
    board.lists.create({'label_id': getLabelId('StatusB::Feedback')})
    board.lists.create({'label_id': getLabelId('StatusB::Suspended')})
    board.lists.create({'label_id': getLabelId('StatusB::Cancelled')})

def create_task_board(title, labels = None):
    board = gitlabGroup.boards.create({'name': title})
    l = 'Type::Task'
    if labels:
        l = l + "," +  ",".join(labels)
    board.labels = l
    board.save()
    board.lists.create({'label_id': getLabelId('StatusT::New')})
    board.lists.create({'label_id': getLabelId('StatusT::Backlog')})
    board.lists.create({'label_id': getLabelId('StatusT::InProgress')})
    board.lists.create({'label_id': getLabelId('StatusT::Done')})
    board.lists.create({'label_id': getLabelId('StatusT::Suspended')})
    board.lists.create({'label_id': getLabelId('StatusT::Cancelled')})

def create_study_board(title, labels = None):
    board = gitlabGroup.boards.create({'name': title})
    l = 'Type::Study'
    if labels:
        l = l + "," +  ",".join(labels)
    board.labels = l
    board.save()
    board.lists.create({'label_id': getLabelId('StatusS::New')})
    board.lists.create({'label_id': getLabelId('StatusS::Backlog')})
    board.lists.create({'label_id': getLabelId('StatusS::InProgress')})
    board.lists.create({'label_id': getLabelId('StatusS::Finished')})
    board.lists.create({'label_id': getLabelId('StatusS::ToRework')})
    board.lists.create({'label_id': getLabelId('StatusS::Validated')})
    board.lists.create({'label_id': getLabelId('StatusS::Suspended')})
    board.lists.create({'label_id': getLabelId('StatusS::Cancelled')})

create_user_story_board('User Stories')
create_bug_board('Bugs')
create_task_board('Tasks')
create_study_board('Studies')

create_user_story_board('User Stories - Release', ['Release::2023-05'])
create_bug_board('Bugs - Release', ['Release::2023-05'])
create_task_board('Tasks - Release', ['Release::2023-05'])
create_study_board('Studies - Release', ['Release::2023-05'])
