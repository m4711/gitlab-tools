import argparse
import re

import requests

from projectLists import tf_projects

# Command line parameters

parser = argparse.ArgumentParser()
parser.add_argument("token", help="GitLab token with API access")
args = parser.parse_args()

def run_query(query):
    request = requests.post(
        "https://gitlab.com/api/graphql", json={"query": query}, headers={"Authorization": "Bearer " + args.token}
    )
    if request.status_code == 200:
        return request.json()
    raise Exception(
        "Query failed to run by returning code of {}. {}".format(
            request.status_code, query
        )
    )

# --------- remove useless labels of merge requests ---------

GET_PROJECT_MR_LIST = """
{
  project(fullPath: "%s") {
    mergeRequests {
      edges {
        node {
          iid
          title
          labels {
            edges {
              node {
                id
                title
              }
            }
          }
        }
      }
    }
  }
}
"""

SET_LABELS_OF_MR ="""
mutation {
  mergeRequestSetLabels(input: {projectPath: "%s", iid: "%s", labelIds: [%s]}) {
    mergeRequest {
      title
      labels {
        nodes {
          id
          title
        }
      }
    }
    errors
  }
}
"""

# regex defining the labels to remove
mrLabelBlacklist = "(Priority::(Lowest|Low|Medium|High)|Status[T,U,B,S]?::.*)"

def set_mr_labels(project, mrIid, labelsIds):
    res = run_query(SET_LABELS_OF_MR % (project, mrIid, ",".join(map(lambda s: '"' + s + '"', labelsIds))))
    if "error" in res:
        raise RuntimeError("Failed to set MR labels: " + res["errors"])

for project in tf_projects:
    print(f"Cleaning {project} project")
    result = run_query(GET_PROJECT_MR_LIST % project)
    for mergeRequests in result["data"]["project"]["mergeRequests"]["edges"]:
        mergeRequest = mergeRequests["node"]
        mrIid = mergeRequest["iid"]
        mrTitle = mergeRequest["title"]
        mrKeptLabelIds = []
        mrHasLabelToRemove = False
        if mergeRequest["labels"]["edges"]:
            print(f"  merge request {mrIid} {mrTitle}")
            for label in mergeRequest["labels"]["edges"]:
                labelTitle = label['node']['title']
                labelIid = label['node']['id']
                if (re.fullmatch(mrLabelBlacklist, labelTitle)):
                    mrHasLabelToRemove = True
                    print(f"    remove label {labelTitle.encode('utf-8')}")
                else:
                    mrKeptLabelIds.append(labelIid)
                    print(f"    keep label {labelTitle.encode('utf-8')}")
            if mrHasLabelToRemove:
                set_mr_labels( project, mrIid, mrKeptLabelIds)
