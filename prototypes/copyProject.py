import sys
import requests
import urllib

if len(sys.argv) != 3:
    print(f'syntax: {sys.argv[0]} <token> <target_namespace>')
    print(f'e.g. {sys.argv[0]} glpat-01234567890123456789 my_group/my_subgroup')
    exit(1)
target_token = sys.argv[1]
target_namespace = sys.argv[2]

source_projects = [ "lmazure_TestGroup/cucumber-discovery",
                    "lmazure_TestGroup/prestashop20220519",
                    "lmazure_TestGroup/secondtestproject" ]

HEADERS = {"Authorization": "Bearer " + target_token}

def fork_project(source_project, target_namespace):
    encoded_project = urllib.parse.quote(source_project, safe="")
    encoded_namespace = urllib.parse.quote(target_namespace, safe="")
    response = requests.post(
        f"https://gitlab.com/api/v4/projects/{encoded_project}/fork?namespace={encoded_namespace}",
        headers = HEADERS,
    )
    if response.status_code != 201:
        print(response.text)
        raise Exception(f"Failed to fork project {source_project} into namespace {target_namespace}: got {response.status_code}." )
    return response.json()["id"]

def remove_fork_relationship(projectId):
    response = requests.delete(
        f"https://gitlab.com/api/v4/projects/{projectId}/fork",
        headers = HEADERS,
    )
    if response.status_code != 204:
        print(response.text)
        raise Exception(f"Failed to delete fork relationship for project {projectId}: got {response.status_code}." )

for source_project in source_projects:
    forked_project_id = fork_project(source_project, target_namespace)
    remove_fork_relationship(forked_project_id)
    print(f"Project {source_project} has been copied into namespace {target_namespace} project {forked_project_id}") 
