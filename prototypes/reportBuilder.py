import argparse
import requests
import urllib.parse


parser = argparse.ArgumentParser()
parser.add_argument("token", help="GitLab token with API access")
args = parser.parse_args()

def makeHeader(token):
    return {"Authorization": "Bearer " + args.token}

def run_post_query(token, query):
    request = requests.post(query, headers=makeHeader(token))
    if request.status_code == 201:
        return
    raise Exception("POST query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

def run_get_query(token, query):
    request = requests.get(query, headers=makeHeader(token))
    if request.status_code == 200:
        return request.json()
    raise Exception("GET query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

def run_put_query(token, query, data = None):
    if data:
        request = requests.put(query, headers=makeHeader(token), data=data)
    else:
        request = requests.put(query, headers=makeHeader(token))
    if request.status_code == 200:
        return
    raise Exception("PUT query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

def run_delete_query(token, query):
    request = requests.delete(query, headers=makeHeader(token))
    if request.status_code in (200, 204):
        return
    raise Exception("DELETE query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))

def run_graphql_query(token, query):
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = makeHeader(token))
    if request.status_code == 200:
        return request.json()
    raise Exception("GraphQL query \"{}\" failed to run by returning code of {}: {}".format(query, request.status_code, request.content))



def create_group_label(token, groupPath, labelName, labelDescription, labelColor, labelPriority):
    params = { "name": labelName, "description": labelDescription, "color": labelColor, "priority": labelPriority }
    query = f"https://gitlab.com/api/v4/groups/{urllib.parse.quote(groupPath, safe='')}/labels?{urllib.parse.urlencode(params)}"
    run_post_query(token, query)
    print(f"label '{labelName}' added on group {groupPath}", flush=True)


def get_group_issues_with_label(token, groupPath, havingOneOfTheLabelName, notHavingTheLabelName):
    issues = []
    page = 1
    while True:
        params = { "labels": havingOneOfTheLabelName, "not[labels]": notHavingTheLabelName, "scope": "all", "page": page } 
        query = f"https://gitlab.com/api/v4/groups/{urllib.parse.quote(groupPath, safe='')}/issues?{urllib.parse.urlencode(params)}"
        result = run_get_query(token, query)
        if len(result) == 0:
            return issues
        for issue in result:
            issues.append({ "projectId": issue["project_id"], "issueIid": issue["iid"], "issueId": issue["id"]})
        page += 1

def add_label_to_issue(token, projectId, issueIid, labelName):
    params = { "add_labels": labelName }
    query = f"https://gitlab.com/api/v4/projects/{projectId}/issues/{issueIid}?{urllib.parse.urlencode(params)}"
    run_put_query(token, query)
    print(f"label '{labelName}' added to issue {issueIid} of project {projectId}", flush=True)

states = {
    "Status::New": [
      "StatusU::New",
      "StatusB::New",
      "StatusT::New",
      "StatusS::New" ],
    "Status::Backlog": [
      "StatusU::Backlog",
      "StatusB::Backlog",
      "StatusT::Backlog",
      "StatusS::Backlog" ],
    "Status::InProgress": [
      "StatusU::ImplemInProgress",
      "StatusB::FixInProgress",
      "StatusT::InProgress",
      "StatusS::InProgress" ],
    "Status::Finished": [
      "StatusU::ReadyForTest",
      "StatusB::ReadyForTest",
      "StatusT::Done",
      "StatusS::Finished" ],
    "Status::SmokeTested": [
      "StatusU::SmokeTested",
      "StatusB::SmokeTested" ],
    "Status::Feedback": [
      "StatusB::Feedback" ],
    "Status::ToBeCorrected": [
      "StatusU::ToBeCorrected",
      "StatusB::ToBeCorrected",
      "StatusS::ToRework" ],
    "Status::Validated": [
      "StatusU::Validated",
      "StatusB::Validated",
      "StatusS::Validated" ],
    "Status::Suspended": [
      "StatusU::Suspended",
      "StatusB::Suspended",
      "StatusT::Suspended",
      "StatusS::Suspended" ],
    "Status::Cancelled": [
      "StatusU::Cancelled",
      "StatusB::Cancelled",
      "StatusT::Cancelled",
      "StatusS::Cancelled" ]
}

def update_group(groupPath):
    for consolidatedState in states:
        # create_group_label(args.token, groupPath, consolidatedState, "", "#FFFFFF", "") # uncomment if the labels do not already exist
        for initialState in states[consolidatedState]:
            for i in  get_group_issues_with_label(args.token, groupPath, initialState, consolidatedState):
                add_label_to_issue(args.token, i["projectId"], i["issueIid"], consolidatedState)

update_group("henixdevelopment/squash")
update_group("henixdevelopment/open-source")
