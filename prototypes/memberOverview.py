import datetime
import os
import requests
import sys
import tempfile

if len(sys.argv) != 2:
    print(f'syntax: {sys.argv[0]} <token>')
    exit(1)
token = sys.argv[1]

HEADERS = {"Authorization": "Bearer " + token}

def run_query(query):
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = HEADERS)
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception(f"Query failed to run by returning code of {request.status_code}. {query}")

all_users = set()
direct_group_membership = set()
direct_project_membership = set()

group_query = """
{
  group(fullPath: "%s") {
    descendantGroups(includeParentDescendants: false) {
      edges {
        node {
          fullPath
          path
        }
      }
    }
    projects {
      edges {
        node {
          fullPath
          path
          projectMembers {
            edges {
              node {
                user {
                  username
                }
                accessLevel {
                  stringValue
                }
              }
            }
          }
        }
      }
    }
    groupMembers {
      edges {
        node {
          user {
            username
          }
          accessLevel {
            stringValue
          }
        }
      }
    }
  }
}
"""

user_query = """
{
  user(username: "%s") {
    groupMemberships {
      edges {
        node {
          group {
            fullPath
          }
        }
      }
    }
    projectMemberships {
      edges {
        node {
          project {
            fullPath
          }
        }
      }
    }
  }
}
"""

# recursively get the members of all groups and projects
def get_group_info(group_path):
    print(group_path, flush=True)
    group_info = { 'name': group_path.split('/')[-1], 'path': group_path, 'subgroups': [], 'projects': [], 'members': {}}
    result = run_query(group_query % group_path)
    for group_node in result["data"]["group"]["descendantGroups"]["edges"]:
        group_info['subgroups'].append(get_group_info(group_node["node"]["fullPath"]))
    for project_node in result["data"]["group"]["projects"]["edges"]:
        project_info = { 'name': project_node["node"]["path"], 'path': project_node["node"]["fullPath"], 'members': {}}
        for member in project_node["node"]["projectMembers"]["edges"]:
            project_info["members"][member["node"]["user"]["username"]] = member["node"]["accessLevel"]["stringValue"]
            all_users.add(member["node"]["user"]["username"])
        group_info["projects"].append(project_info)
    for member in result["data"]["group"]["groupMembers"]["edges"]:
        group_info["members"][member["node"]["user"]["username"]] = member["node"]["accessLevel"]["stringValue"]
        all_users.add(member["node"]["user"]["username"])
    return group_info

root = get_group_info("henixdevelopment")

sorted_users = sorted(all_users, key=str.lower)

# complete the information with the direct memberships
# (the request must be done on the users…)
for user in sorted_users:
    print(user, flush=True)
    result = run_query(user_query % user)
    for group_node in result["data"]["user"]["groupMemberships"]["edges"]:
        group = group_node["node"]["group"]["fullPath"]
        direct_group_membership.add((group, user))
    for project_node in result["data"]["user"]["projectMemberships"]["edges"]:
        project = project_node["node"]["project"]["fullPath"]
        direct_project_membership.add((project, user))

# generate the HMTL report
def dump_projet_info(project_info, report, depth):
    report.write('<tr><td>' +
                 ('&nbsp;&nbsp;' * (depth - 1)) +
                 '&#9655;&nbsp;<a href="https://gitlab.com/' +
                 project_info["path"] +
                 '/-/project_members" target="_blank">' +
                 project_info["name"] +
                 '</a></td>')
    for user in sorted_users:
        if user in project_info["members"]:
            directFlag = " DIRECT" if (project_info["path"], user) in direct_project_membership else ""
            report.write('<td class="' + project_info["members"][user] + directFlag + '">' + project_info["members"][user][0] + "</td>")
        else:
            report.write("<td></td>")
    report.write("</tr>\n")

def dump_group_info(group_info, report, depth):
    report.write('<tr><td>' +
                 ('&nbsp;&nbsp;' * (depth - 1)) +
                 ('&#9654;&nbsp;' if (depth > 0) else '') +
                 '<a href="https://gitlab.com/groups/' +
                 group_info["path"] +
                 '/-/group_members" target="_blank">' +
                 group_info["name"] +
                 '</a></td>')
    for user in sorted_users:
        if user in group_info["members"]:
            directFlag = " DIRECT" if (group_info["path"], user) in direct_group_membership else ""
            report.write('<td class="' + group_info["members"][user] + directFlag + '">' + group_info["members"][user][0] + "</td>")
        else:
            report.write("<td></td>")
    report.write("</tr>\n")
    for subgroup in sorted(group_info["subgroups"], key=lambda g: g['name']):
        dump_group_info(subgroup, report, depth + 1)
    for project in sorted(group_info["projects"], key=lambda p: p['name']):
        dump_projet_info(project, report, depth + 1)

report = tempfile.NamedTemporaryFile(mode="w", suffix=".html", delete=False)

report.write('<!DOCTYPE html>\n<html>\n<head>\n<meta charset="utf-8">\n<style>\n'
             'table { table-layout: fixed; width: 100%}\n'
             'th { writing-mode: vertical-lr; text-orientation: sideways; text-align: center;}\n'
             '.GUEST { background-color: LightPink; }\n'
             '.REPORTER { background-color: LightGreen; }\n'
             '.DEVELOPER { background-color: LemonChiffon; }\n'
             '.MAINTAINER { background-color: Gold; }\n'
             '.OWNER { background-color: Tomato; }\n'
             '.DIRECT { text-decoration: underline overline; font-weight: bolder; }\n'
             'table { border-spacing:0; }\n'
             'td { border-top: 1px solid white; border-bottom: 1px solid white; }\n'
             'tbody tr:hover td { border-color: black; }\n'
             '</style>\n'
             '</head>\n'
             '<body>\n'
             '<script>\n'
             'let total = 0, nbCol;\n'
             'function draw() {\n'
             '    for (i = 1; i < nbCol; i++) {\n'
             '        const elements = document.querySelectorAll("tr>:nth-child(" + (i + 1) + ")");\n'
             '        for (j = 2; j < elements.length; j++) {\n'
             '            elements[j].style.filter = ((total > 0) && !document.getElementById("cb" + i).checked) ? "sepia(90%)"\n'
             '                                                                                                   : "sepia(0%)";\n'
             '        }\n'
             '    }\n'
             '}\n'
             'function handle(element) {\n'
             '    total += (element.checked) ? 1 : -1;\n'
             '    draw();\n'
             '}\n'
             'window.onload = (event) => {\n'
             '    nbCol = document.querySelectorAll("th").length;\n'
             '    draw();\n'
             '};\n'
             '</script>\n'
             'This report has been generated on ' +
             datetime.datetime.now().astimezone().isoformat() +
             ' by <a href="https://gitlab.com/m4711/gitlab-tools/-/blob/main/memberOverview.py" target="_blank"">https://gitlab.com/m4711/gitlab-tools/-/blob/main/memberOverview.py</a>.<br>\n' +
             'The GitLab roles and permissions are described in <a href="https://docs.gitlab.com/ee/user/permissions.html" target="_blank"">https://docs.gitlab.com/ee/user/permissions.html</a>.<br>\n' +
             '<table>\n')

report.write('<colgroup><col style="width: 15px;"></col>')
for user in sorted_users:
    report.write('<col style="width: 1px;"></col>')
report.write('</colgroup>\n')

report.write("<tr><th></th>")
for user in sorted_users:
    report.write("<th>" + user + "</th>")
report.write("</tr>\n")

report.write("<tr><td></td>")
i = 1
for user in sorted_users:
    report.write('<td><input type="checkbox" id="cb' + str(i) + '" onchange="handle(this)"></td>')
    i = i + 1
report.write("</tr>\n")

dump_group_info(root, report, 0)
report.write("</table>\n</body>\n</html>")

os.startfile(report.name)
