# My GitLab tools

Quick 'n dirty tools for GitLab usage

## createMilestone.py
Create a milestone in the Squash groups.

```bash
python createMilestone.py $GITLAB_TOKEN "Sprint 42" "1970-01-01" "1970-01-15"
```

## createLabel.py
Create a label in the projects/groups used by Squash.
```bash
python createLabel.py $GITLAB_TOKEN "name" "description" "color" "priority"
```
"description" and "color" can be empty.  
For example, to create a new release label:
```bash
python createLabel.py $GITLAB_TOKEN "Release::6.0.1" "" "#bbbbff" ""
```

## moveLabelHigher.py
Create a label on a group and replace the labels of the same name which are defined further down in the group/project hierarchy.

## memberOverview.py
Generate and display (in the Browser) an HTML overview of the members of all groups and projects in https://gitlab.com/groups/henixdevelopment.

```bash
python memberOverview.py $GITLAB_TOKEN
```

## projectDumper.py
List parameters of the projects:

- generates a HTML report
- used interactively, displays the report in the Browser

(The project list is defined in `projectList.py`.)

```bash
python projectDumper.py $GITLAB_TOKEN
```

## statMergedMRForUser.py
Compute the weekly number of merged MR merged by a given user
```bash
 python statMergedMRForUser.py $GITLAB_TOKEN lmazure
```

## find_issues_from_external_users.py
List the issues created by external users
`GITLAB_TOKEN` environment variable must contain your GitLab API key
```sh
python find_issues_from_external_users.py
```

## dockerPullCounter.py + dockerPullCounter.sh
Extract the number of pulls of some Docker images from https://hub.docker.com/.  
The results are stored in https://gitlab.com/m4711/data-store.

## clocgit.sh
Count the number of lines in each Git repo.  
(The project list is hardcoded in the script.)

## findLabel.py
Find where is defined (group and/or project) a label.

## changeIssueMilestone
Move all Squash issues from a given milestone to another one.

```bash
python changeIssueMilestone.py $GITLAB_TOKEN "Sprint 42" "Sprint 43"
```
A (GraphQL) filter can be defined:

```bash
python changeIssueMilestone.py $GITLAB_TOKEN "Sprint 42" "Sprint 43" 'labelName: "Team::Other"'
```

## plausible.py
Create an Excel doc containing the Plausible statistics of https://squashtest.com.

```bash
python plausible.py $PLAUSIBLE_TOKEN foobar.xslx
```


---

# ARCHIVE

## checklist.py
Create issues for tracking all communication tasks necessary for
- a Squash TM release
- an AUTOM/DEVOPS release

(Issues are created in [https://gitlab.com/henixdevelopment/squash/communication/suivi](https://gitlab.com/henixdevelopment/squash/communication/suivi/-/boards/3168308).)


---

# DO NOT USE

## labels.sh
Create labels in a project.  
⚠️⚠️ Edit file before executing it so it does what you want! ⚠️⚠️

## board.py
Create an issue board.  
⚠️WIP, do not use

## setOpentestfactoryBoard.py
Configure the issue board for https://gitlab.com/opentestfactory.  
⚠️ https://gitlab.com/opentestfactory does not exist anymore

